/**
 * @file
 * JavaScript to add search filter to table.
 */
(function ($) {

  "use strict";

  Drupal.behaviors.edgeExperiences = {
    attach: function (context) {
      $(document).ready(function() {

        $('#uw_edge_experiences').before('<label for="uw_edge_search">Search experiences: </label><input type="text" id="uw_edge_search">');

        $("#uw_edge_search").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#uw_edge_experiences > tbody tr").filter(function() {
            $(this).toggle($(this).find(".tablesaw-cell-content").text().toLowerCase().indexOf(value) > -1)
          });
        });

      });//End ready.
    }//End attach.
  };
}(jQuery));